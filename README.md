# README #

A libretro core exposing OpenGL ES through Luajit. Portable, mostly targeted to Pi 3 and 0.

## Build

`make platform=pi`

## Run

`/opt/retropie/emulators/retroarch/bin/retroarch -L jouleplating_libretro.so /home/pi/code/git/libretro-jouleplating/scene_es2/simple_sprite.lua`


## Install
Copy the `installscripts` directory over to a share on a new Retropie image. Quit emulationstation and run it from the shell:
```
chmod a+x add-script-to-retropie-setup.sh
./add-script-to-retropie-setup.sh
```
Once the retropie script module for jouleplating is copied in, run retropie-setup normally. Install lr-jouleplating from the exp menu:
```
P Manage packages
exp Manage experminental packages
lr-jouleplating
<  OK  >
```
`emulators.cfg` will be created in `/opt/retropie/configs/jouleplating`

When the roms directory for the new "system" has been created, copy in all the code and data:
```
cp -Rv data $ROMS_HOME/
cp -Rv util $ROMS_HOME/
cp -Rv scene_es2/* $ROMS_HOME/
```
Add in any new games by copying files in. TODO: that's a bit sloppy, maybe we can package these modules in zip files like love.

### Startup files:
`jouleplating.lua`
`opengles2.lua`

### Install Dir
`/opt/retropie/libretrocores/lr-jouleplating/`
`/opt/retropie/configs/jouleplating/`

### Roms Dir
`/home/pi/RetroPie/roms/jouleplating/`


## On Desktop Linux
`ln -s LuaJIT/src/libluajit.so libluajit-5.1.so.2`

## On Windows
`cp LuaJIT/src/lua51.dll .`
