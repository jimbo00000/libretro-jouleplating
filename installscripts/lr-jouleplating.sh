#!/usr/bin/env bash

# This file is part of The RetroPie Project
#
# The RetroPie Project is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/RetroPie/RetroPie-Setup/master/LICENSE.md
#

rp_module_id="lr-jouleplating"
rp_module_desc="JoulePlating - LuaJIT/OpenGL game framework"
rp_module_help="'ROM' Extensions: .lua\n\nCopy your scene files to $romdir\n\nCopy your data files to $romdir/data"
rp_module_licence="MIT"
rp_module_section="exp"

function sources_lr-jouleplating() {
    gitPullOrClone "$md_build" https://bitbucket.org/jimbo00000/libretro-jouleplating.git
    cp -vR "$md_build/themes" /etc/emulationstation/
}

function build_lr-jouleplating() {
    CFLAGS="" make -C LuaJIT
    if isPlatform "rpi"; then
        CFLAGS="" make platform=pi
    else
        make
    fi
    md_ret_require="$md_build/jouleplating_libretro.so"
}

function install_lr-jouleplating() {
    md_ret_files=(
        'jouleplating_libretro.so'
        'jouleplating.lua'
        'opengles2.lua'
    )
}

function configure_lr-jouleplating() {
    mkRomDir "jouleplating"
    ensureSystemretroconfig "jouleplating"
    addEmulator 1 "$md_id" "jouleplating" "$md_inst/jouleplating_libretro.so" 
    addSystem "jouleplating" "Jouleplating" ".lua"

    setRetroArchCoreOption "jouleplating_startupdir" "pi"
    setRetroArchCoreOption "jouleplating_codedir" "pi"
    setRetroArchCoreOption "jouleplating_datadir" "pi"
    setRetroArchCoreOption "jouleplating_resolution" "320x240"
}
