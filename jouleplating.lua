-- jouleplating.lua

local ffi = require 'ffi'
local openGL

-- Note package.path will have the startup file's path appended.
print(jit.version, jit.os, jit.arch)
print('package.path',package.path)
print('package.cpath',package.cpath)
print('datadir',datadir)

local win_w,win_h
local scene_name = nil
local Scene
local datadir = ""

function lua_set_directories(startupPath, dataPath, codePath)
    print("___lua_set_directories___")
    print("startupPath=",startupPath)
    print("dataPath=",dataPath)
    print("codePath=",codePath)
    datadir = dataPath

    package.path = package.path .. ';' .. codePath .. '/?.lua'

    local filesystem = require 'util.filesystem'
    filesystem.dataDirectory = datadir
end

function switch_to_scene(name)
    local status,err = pcall(function ()
        print('switch_to_scene - require', name)
        local SceneLibrary = require(name)
        Scene = SceneLibrary.new()
    end)
    if not status then
        print('Error in require:', err)
        os.exit(0)
    end
    if not Scene then
        print('Scene library returned nil.')
        os.exit(0)
    end

    if Scene.setDataDirectory then Scene:setDataDirectory(datadir) end
    if Scene.resizeViewport then Scene:resizeViewport(win_w,win_h) end
    Scene:initGL()
end

function on_lua_change_game(name)
    -- Parse out path, filename, extension
    -- Thank you Paul Kulchenko
    local p,f,e = string.match(name, "(.-)([^\\/]-%.?([^%.\\/]*))$")
    print(name,p,f,e)
    if e == "lua" then
        local scene = f:sub(1,-5)
        print("Switch to scene: ",scene)
        scene_name = scene

        package.path = package.path .. ';' .. p .. '/?.lua'
    end
end

-- Typically called on launch before initgl
function on_lua_resize_viewport(w,h)
    win_w,win_h = w,h
end

function on_lua_initgl(pLoaderFunc, isGLES)
    if pLoaderFunc == 0 then
        print("No GL loader function passed in.")
        return
    else
        print("GL loader func:", bit.tohex(pLoaderFunc))
        ffi.cdef[[
        typedef void (*GLFWglproc)();
        GLFWglproc glfwGetProcAddress(const char* procname);
        typedef GLFWglproc (*GLFWGPAProc)(const char*);
        ]]
        if isGLES == 1 then
            openGL = require("opengles2")
        else
            openGL = require("opengl")
        end
        openGL.loader = ffi.cast('GLFWGPAProc', pLoaderFunc)
    end
    openGL:import()

    package.loaded.opengl = openGL

    print(ffi.string(gl.glGetString(GL.GL_VENDOR)))
    print(ffi.string(gl.glGetString(GL.GL_RENDERER)))
    print(ffi.string(gl.glGetString(GL.GL_VERSION)))
    print(ffi.string(gl.glGetString(GL.GL_SHADING_LANGUAGE_VERSION)))
    -- TODO: check version and optionally create VAO

    switch_to_scene(scene_name)
end

function on_lua_exitgl()
end

function on_lua_drawframe(frame_count)
    local frame_rate = 60 -- hz

    local status, err = pcall(function ()
        if Scene.render then Scene:render() else
        Scene:render_for_one_eye(v,p) end
    end)
    if not status then print(err) end

    local t_seconds = frame_count/frame_rate
    Scene:timestep(t_seconds, 1/frame_rate)
end

function array_to_table(array)
    local m0 = ffi.cast("short*", array)
    -- The cdata array is 0-indexed. Here we clumsily jam it back
    -- into a Lua-style, 1-indexed table(array portion).
    local tab = {}
    for i=0,13 do tab[i+1] = m0[i] end
    return tab
end

local retropadStates = {}
function on_lua_joystick(jlist)

    local lastStates = {}
    for _,v in pairs(retropadStates) do
        local last = {}
        for i=1,14 do last[i] = v[i] end
        table.insert(lastStates, last)
    end
    retropadStates = {}

    for _,v in pairs(jlist) do
        table.insert(retropadStates, array_to_table(v))
    end

    for i=1,#lastStates do
        retropadStates[i].last = lastStates[i]
    end
    
    if Scene.update_retropad then
        Scene:update_retropad(retropadStates)
    end

end
