#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <lua.h>
#include "lauxlib.h"

#include "glsym/glsym.h"

#include "audio.h"
#include "libretro.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
static struct retro_hw_render_callback hw_render;

#if defined(HAVE_PSGL)
#define RARCH_GL_FRAMEBUFFER GL_FRAMEBUFFER_OES
#define RARCH_GL_FRAMEBUFFER_COMPLETE GL_FRAMEBUFFER_COMPLETE_OES
#define RARCH_GL_COLOR_ATTACHMENT0 GL_COLOR_ATTACHMENT0_EXT
#elif defined(OSX_PPC)
#define RARCH_GL_FRAMEBUFFER GL_FRAMEBUFFER_EXT
#define RARCH_GL_FRAMEBUFFER_COMPLETE GL_FRAMEBUFFER_COMPLETE_EXT
#define RARCH_GL_COLOR_ATTACHMENT0 GL_COLOR_ATTACHMENT0_EXT
#else
#define RARCH_GL_FRAMEBUFFER GL_FRAMEBUFFER
#define RARCH_GL_FRAMEBUFFER_COMPLETE GL_FRAMEBUFFER_COMPLETE
#define RARCH_GL_COLOR_ATTACHMENT0 GL_COLOR_ATTACHMENT0
#endif

#define BASE_WIDTH 320
#define BASE_HEIGHT 240
#ifdef HAVE_OPENGLES
#define MAX_WIDTH 1024
#define MAX_HEIGHT 1024
#else
#define MAX_WIDTH 2048
#define MAX_HEIGHT 2048
#endif

lua_State* m_Lua = NULL;

// Config options
static unsigned width  = BASE_WIDTH;
static unsigned height = BASE_HEIGHT;
static int whichStartupDir = 0;
static int whichDataDir = 0;
static int whichCodeDir = 0;

static bool use_audio_cb;
int16_t audio_buffer[2 * AUDIO_FRAMES];

static retro_audio_sample_t audio_cb;
static retro_audio_sample_batch_t audio_batch_cb;

static struct retro_log_callback logging;
static retro_log_printf_t log_cb;

static void fallback_log(enum retro_log_level level, const char *fmt, ...)
{
   (void)level;
   va_list va;
   va_start(va, fmt);
   vfprintf(stderr, fmt, va);
   va_end(va);
}

static void emit_audio(void)
{
   mixer_render(audio_buffer);
   audio_batch_cb(audio_buffer, AUDIO_FRAMES);
}

static void audio_set_state(bool enable)
{
   (void)enable;
}

#if defined(CORE)
static bool context_alive;
static bool multisample_fbo;
static unsigned multisample;
#endif

#if defined(CORE)
static void init_multisample(unsigned samples)
{
   multisample = samples;
   if (!context_alive)
      return;

   if (rbo_color)
      glDeleteRenderbuffers(1, &rbo_color);
   if (rbo_depth_stencil)
      glDeleteRenderbuffers(1, &rbo_depth_stencil);
   if (fbo)
      glDeleteFramebuffers(1, &fbo);

   rbo_color = rbo_depth_stencil = fbo = 0;
   multisample_fbo = false;
   if (samples <= 1)
      return;

   if (glRenderbufferStorageMultisample)
   {
      glGenRenderbuffers(1, &rbo_color);
      glGenRenderbuffers(1, &rbo_depth_stencil);
      glGenFramebuffers(1, &fbo);

      glBindRenderbuffer(GL_RENDERBUFFER, rbo_color);
      glRenderbufferStorageMultisample(GL_RENDERBUFFER,
            samples, GL_RGBA, MAX_WIDTH, MAX_HEIGHT);
      glBindRenderbuffer(GL_RENDERBUFFER, rbo_depth_stencil);
      glRenderbufferStorageMultisample(GL_RENDERBUFFER,
            samples, GL_DEPTH24_STENCIL8, MAX_WIDTH, MAX_HEIGHT);
      glBindRenderbuffer(GL_RENDERBUFFER, 0);

      glGenFramebuffers(1, &fbo);
      glBindFramebuffer(RARCH_GL_FRAMEBUFFER, fbo);

      glFramebufferRenderbuffer(RARCH_GL_FRAMEBUFFER, RARCH_GL_COLOR_ATTACHMENT0,
            GL_RENDERBUFFER, rbo_color);
      glFramebufferRenderbuffer(RARCH_GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT,
            GL_RENDERBUFFER, rbo_depth_stencil);

      GLenum ret = glCheckFramebufferStatus(RARCH_GL_FRAMEBUFFER);
      if (ret == RARCH_GL_FRAMEBUFFER_COMPLETE)
      {
         fprintf(stderr, "[JOULEPLATING] Using multisampled FBO.\n");
         multisample_fbo = true;
      }
      else
         fprintf(stderr, "[JOULEPLATING] Multisampled FBO failed.\n");

      glBindFramebuffer(RARCH_GL_FRAMEBUFFER, 0);
   }
   else
      fprintf(stderr, "[JOULEPLATING] Multisampled FBOs not supported.\n");
}
#endif

// http://stackoverflow.com/questions/4508119/redirecting-redefining-print-for-embedded-lua
static int l_my_print(lua_State* L) {
   int nargs = lua_gettop(L);

   fprintf(stderr, "[LUAJIT]: ");
   for (int i=1; i <= nargs; i++) {
      if (lua_isstring(L, i)) {
         const char* pStr = lua_tostring(L, i);
         fprintf(stderr, "%s\t", pStr);
      }
      else {
         /* Do something with non-strings if you like */
      }
   }
   fprintf(stderr, "\n");

   return 0;
}

static const struct luaL_Reg printlib [] = {
    {"print", l_my_print},
    {NULL, NULL} /* end of array */
};

extern void luaopen_luamylib(lua_State *L)
{
    lua_getglobal(L, "_G");
    luaL_register(L, NULL, printlib);
    lua_pop(L, 1);
}

void retro_init(void)
{
   fprintf(stderr, "[JOULEPLATING] retro_init\n");
}

bool lua_init(const char* gamePath)
{
   fprintf(stderr, "[JOULEPLATING] lua_init(%s)\n", gamePath);

   m_Lua = luaL_newstate();

   luaL_openlibs(m_Lua);
   if (m_Lua == NULL)
   {
      log_cb(RETRO_LOG_ERROR, "[JOULEPLATING] ERROR: lua open libs\n");
   }

   lua_State *L = m_Lua;
   luaopen_luamylib(L);
   
   // set path for jouleplating.lua and opengles2.lua
   const char* dotStartup = ".";
   const char* gameStartup = ".";
   const char* owlStartup = "../opengl-with-luajit";
   const char* piStartup = "/opt/retropie/libretrocores/lr-jouleplating";
   const char* devStartup = "";

   const char* dotCode = ".";
   const char* gameCode = ".";
   const char* owlCode = "../opengl-with-luajit";
   const char* piCode = "/home/pi/RetroPie/roms/jouleplating";
   const char* devCode = "";

   const char* dotData = "./data";
   const char* gameData = "./data";
   const char* owlData = "../opengl-with-luajit/data";
   const char* piData = "/home/pi/RetroPie/roms/jouleplating/data";
   const char* devData = ".";
   
   const char* startupPath = piStartup;
   const char* codePath = piCode;
   const char* dataPath = piData;

   switch (whichStartupDir)
   {
      case 1: startupPath = dotStartup; break;
      case 2: startupPath = gameStartup; break;
      case 3: startupPath = owlStartup; break;
      case 4: startupPath = piStartup; break;
      case 5: startupPath = devStartup; break;
      case 0: default: break;
   }

   switch (whichCodeDir)
   {
      case 1: codePath = dotCode; break;
      case 2: codePath = gameCode; break;
      case 3: codePath = owlCode; break;
      case 4: codePath = piCode; break;
      case 5: codePath = devCode; break;
      case 0: default: break;
   }

   switch (whichDataDir)
   {
      case 1: dataPath = dotData; break;
      case 2: dataPath = gameData; break;
      case 3: dataPath = owlData; break;
      case 4: dataPath = piData; break;
      case 5: dataPath = devData; break;
      case 0: default: break;
   }

   log_cb(RETRO_LOG_INFO, "[JOULEPLATING] setting startup path=%s\n", startupPath);
   
   char strbuf[512];
   snprintf(strbuf, 512, "package.path = package.path .. ';%s/?.lua'", startupPath);
   luaL_dostring(L, strbuf);
   luaL_dostring(L, "print(package.path)");

   snprintf(strbuf, 512, "datadir = '%s'", dataPath);
   luaL_dostring(L, strbuf);

   snprintf(strbuf, 512, "%s/jouleplating.lua", startupPath);
   log_cb(RETRO_LOG_INFO, "[JOULEPLATING] luaL_loadfile(%s)\n", strbuf);
   
   int errCode = luaL_loadfile(L, strbuf);
   switch (errCode)
   {
      case 0:
      break;
      
      case LUA_ERRSYNTAX:
      log_cb(RETRO_LOG_ERROR, "[JOULEPLATING] LUA_ERRSYNTAX\n");
      return false;
      break;

      case LUA_ERRMEM:
      log_cb(RETRO_LOG_ERROR, "[JOULEPLATING] LUA_ERRMEM\n");
      return false;
      break;

      case LUA_ERRFILE:
      log_cb(RETRO_LOG_ERROR, "[JOULEPLATING] LUA_ERRFILE - %s not found.\n", strbuf);
      return false;
      break;

      default:
      log_cb(RETRO_LOG_ERROR, "[JOULEPLATING] Unknown error\n");
      return false;
      break;
   }

   log_cb(RETRO_LOG_INFO, "[JOULEPLATING] lua_pcall\n");
   errCode = lua_pcall(L, 0, LUA_MULTRET, 0);
   switch (errCode)
   {
      case 0:
      break;
      
      case LUA_ERRRUN:
      log_cb(RETRO_LOG_ERROR, "[JOULEPLATING] LUA_ERRRUN\n");
      return false;
      break;

      case LUA_ERRMEM:
      log_cb(RETRO_LOG_ERROR, "[JOULEPLATING] LUA_ERRMEM\n");
      return false;
      break;

      case LUA_ERRERR:
      log_cb(RETRO_LOG_ERROR, "[JOULEPLATING] LUA_ERRERR\n");
      return false;
      break;

      default:
      log_cb(RETRO_LOG_ERROR, "[JOULEPLATING] pcall Unknown error\n");
      return false;
      break;
   }

   // Inform Lua of paths to code and data
   lua_getglobal(L, "lua_set_directories");
   lua_pushstring(L, startupPath);
   lua_pushstring(L, dataPath);
   lua_pushstring(L, codePath);
   if (lua_pcall(L, 3, 0, 0) != 0)
   {
      log_cb(RETRO_LOG_ERROR, "[JOULEPLATING] ERROR in lua_set_directories: %s\n", lua_tostring(L, -1));
      return false;
   }

   
   // Inform Lua of game name
   lua_getglobal(L, "on_lua_change_game");
   lua_pushstring(L, gamePath);
   if (lua_pcall(L, 1, 0, 0) != 0)
   {
      log_cb(RETRO_LOG_ERROR, "[JOULEPLATING] ERROR in on_lua_change_game: %s\n", lua_tostring(L, -1));
      return false;
   }

   lutro_audio_preload(L);
   return true;
}

void retro_deinit(void)
{
   if (m_Lua != NULL)
   {
      lua_close(m_Lua);
   }
}

unsigned retro_api_version(void)
{
   return RETRO_API_VERSION;
}

void retro_set_controller_port_device(unsigned port, unsigned device)
{
   (void)port;
   (void)device;
}

void retro_get_system_info(struct retro_system_info *info)
{
   memset(info, 0, sizeof(*info));
   info->library_name     = "JoulePlating";
   info->library_version  = "v1";
   info->need_fullpath    = false;
   info->valid_extensions = NULL; // Anything is fine, we don't care.
}

void retro_get_system_av_info(struct retro_system_av_info *info)
{
   fprintf(stderr, "[JOULEPLATING] retro_get_system_av_info\n");

   info->timing = (struct retro_system_timing) {
      .fps = 60.0,
      .sample_rate = 44100.0,
   };

   info->geometry = (struct retro_game_geometry) {
      .base_width   = width,
      .base_height  = height,
      .max_width    = MAX_WIDTH,
      .max_height   = MAX_HEIGHT,
      .aspect_ratio = (float)width / (float)height,
   };
}

static retro_video_refresh_t video_cb;
static retro_audio_sample_t audio_cb;
static retro_audio_sample_batch_t audio_batch_cb;
static retro_environment_t environ_cb;
static retro_input_poll_t input_poll_cb;
static retro_input_state_t input_state_cb;

void retro_set_environment(retro_environment_t cb)
{
   fprintf(stderr, "[JOULEPLATING] retro_set_environment\n");

   environ_cb = cb;

   struct retro_variable variables[] = {
      {
         "jouleplating_resolution",

#ifdef HAVE_OPENGLES
         "Internal resolution; 320x240|360x480|480x272|512x384|512x512|640x240|640x448|640x480|720x576|800x600|960x720|1024x768",
#else
         "Internal resolution; 320x240|360x480|480x272|512x384|512x512|640x240|640x448|640x480|720x576|800x600|960x720|1024x768|1024x1024|1280x720|1280x960|1600x1200|1920x1080|1920x1440|1920x1600|2048x2048",
#endif
      },
#ifdef CORE
      { "testgl_multisample", "Multisampling; 1x|2x|4x" },
#endif

      { "jouleplating_startupdir", "Startup directory; .|game|owl|pi|dev" },
      { "jouleplating_codedir", "Code directory; .|game|owl|pi|dev" },
      { "jouleplating_datadir", "Data directory; .|game|owl|pi|dev" },

      { NULL, NULL },
   };

   bool no_rom = false;
   cb(RETRO_ENVIRONMENT_SET_SUPPORT_NO_GAME, &no_rom);
   cb(RETRO_ENVIRONMENT_SET_VARIABLES, variables);

#if 0
   // Use retroarch's logging
   if (cb(RETRO_ENVIRONMENT_GET_LOG_INTERFACE, &logging))
      log_cb = logging.log;
   else
#endif
      log_cb = fallback_log;
}

int getPathEnumFromString(const char* pStr)
{
   if (!strcmp(pStr, "."))
   {
      return 1;
   }
   else if (!strcmp(pStr, "game"))
   {
      return 2;
   }
   else if (!strcmp(pStr, "owl"))
   {
      return 3;
   }
   else if (!strcmp(pStr, "pi"))
   {
      return 4;
   }
   else if (!strcmp(pStr, "dev"))
   {
      return 5;
   }
   else
   {
      return 0;
   }
}

static void update_variables(void)
{
   struct retro_variable var = {
      .key = "jouleplating_resolution",
   };

   if (environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
   {
      char *pch;
      char str[100];
      snprintf(str, sizeof(str), "%s", var.value);
      
      pch = strtok(str, "x");
      if (pch)
         width = strtoul(pch, NULL, 0);
      pch = strtok(NULL, "x");
      if (pch)
         height = strtoul(pch, NULL, 0);

      log_cb(RETRO_LOG_ERROR, "[JOULEPLATING]: Got size: %u x %u.\n", width, height);
   }

#ifdef CORE
   var.key = "gl_multisample";
   var.value = NULL;

   if (environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
   {
      switch (*var.value)
      {
         case '1':
            init_multisample(1);
            break;

         case '2':
            init_multisample(2);
            break;

         case '4':
            init_multisample(4);
            break;
      }
   }
#endif

   var.key = "jouleplating_startupdir";
   var.value = NULL;
   if (environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
   {
      whichStartupDir = getPathEnumFromString(var.value);
      log_cb(RETRO_LOG_INFO, "[JOULEPLATING]: startupDir=[%s](%d)\n", var.value, whichStartupDir);
   }

   var.key = "jouleplating_codedir";
   var.value = NULL;
   if (environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
   {
      whichCodeDir = getPathEnumFromString(var.value);
      log_cb(RETRO_LOG_INFO, "[JOULEPLATING]: codeDir=[%s](%d)\n", var.value, whichCodeDir);
   }

   var.key = "jouleplating_datadir";
   var.value = NULL;
   if (environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
   {
      whichDataDir = getPathEnumFromString(var.value);
      log_cb(RETRO_LOG_INFO, "[JOULEPLATING]: dataDir=[%s](%d)\n", var.value, whichDataDir);
   }
}

void retro_set_audio_sample(retro_audio_sample_t cb)
{
   audio_cb = cb;
}

void retro_set_audio_sample_batch(retro_audio_sample_batch_t cb)
{
   audio_batch_cb = cb;
}

void retro_set_input_poll(retro_input_poll_t cb)
{
   input_poll_cb = cb;
}

void retro_set_input_state(retro_input_state_t cb)
{
   input_state_cb = cb;
}

void retro_set_video_refresh(retro_video_refresh_t cb)
{
   video_cb = cb;
}

static unsigned frame_count;
#define NUM_CONTROLLERS 4
#define NUM_BUTTONS 14
static int16_t joystick_cache[NUM_CONTROLLERS][NUM_BUTTONS] = {0};
static uint8_t joystick_used[NUM_CONTROLLERS] = {0};

void joystick_event(void)
{
   int i, u;
   int16_t state;

   ///@todo How can a core find out from retroarch how many controllers are connected?
   //joystick_used[0] = 1;

   // Loop through each joystick.
   for (i = 0; i < NUM_CONTROLLERS; i++) {
      // Loop through each button.
      for (u = 0; u < NUM_BUTTONS; u++) {
         // Retrieve the state of the button.
         state = input_state_cb(i, RETRO_DEVICE_JOYPAD, 0, u);
         if (joystick_cache[i][u] != state)
         {
            joystick_used[i] = 1;
         }
         joystick_cache[i][u] = state;
      }
   }

   // Inform Lua of joystick state
   int njoys = 0;
   for (i = 0; i < NUM_CONTROLLERS; i++)
   {
      if (joystick_used[i])
      {
         ++njoys;
      }
   }

   lua_State* L = m_Lua;
   lua_getglobal(L, "on_lua_joystick");

   lua_createtable(L, njoys, 0);

   int newTable = lua_gettop(L);
   for (i = 0; i < njoys; i++)
   {
      lua_pushlightuserdata(L, (void*)(joystick_cache[i]));
      lua_rawseti(L, newTable, i + 1);
   }

   if (lua_pcall(L, 1, 0, 0) != 0)
   {
      fprintf(stderr, "[JOULEPLATING] ERROR in on_lua_joystick: %s\n", lua_tostring(L, -1));
   }
}

void retro_run(void)
{
   bool updated = false;
   if (environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE_UPDATE, &updated) && updated)
   {
      fprintf(stderr, "[JOULEPLATING] retro_run env cb!\n");
      update_variables();
   }

   input_poll_cb();

#ifdef CORE
   glBindVertexArray(vao);
   if (multisample_fbo)
      glBindFramebuffer(RARCH_GL_FRAMEBUFFER, fbo);
   else
#endif
      glBindFramebuffer(RARCH_GL_FRAMEBUFFER, hw_render.get_current_framebuffer());

   glClearColor(0.3, 0.4, 0.5, 1.0);
   glViewport(0, 0, width, height);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

   lua_State* L = m_Lua;
   lua_getglobal(L, "on_lua_drawframe");
   lua_pushnumber(L, frame_count);
   if (lua_pcall(L, 1, 0, 0) != 0)
   {
      fprintf(stderr, "[JOULEPLATING] ERROR in on_lua_drawframe: %s\n", lua_tostring(L, -1));
   }

#ifdef CORE
   glBindVertexArray(0);
   if (multisample_fbo) // Resolve the multisample.
   {
      glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, hw_render.get_current_framebuffer());
      glBlitFramebuffer(0, 0, width, height,
            0, 0, width, height,
            GL_COLOR_BUFFER_BIT, GL_NEAREST);
      glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
   }
#endif

   joystick_event();
   frame_count++;

   video_cb(RETRO_HW_FRAME_BUFFER_VALID, width, height, 0);
}

static void context_reset(void)
{
   //fprintf(stderr, "Context reset!\n");
   rglgen_resolve_symbols(hw_render.get_proc_address);
 
   lua_State* L = m_Lua;

   // Inform Lua of viewport size
   lua_getglobal(L, "on_lua_resize_viewport");
   lua_pushnumber(L, width);
   lua_pushnumber(L, height);
   if (lua_pcall(L, 2, 0, 0) != 0)
   {
      fprintf(stderr, "[JOULEPLATING] ERROR in on_lua_resize_viewport: %s\n", lua_tostring(L, -1));
   }

   lua_getglobal(L, "on_lua_initgl");
   // Pass in a (GL function loader) function pointer.
   lua_Number LpLoaderFunc = (double)((intptr_t)hw_render.get_proc_address);
   lua_pushnumber(L, LpLoaderFunc);

   // Choose desktop or ES OpenGL
   int isGLES = 0;
#ifdef HAVE_OPENGLES
   isGLES = 1;
#endif
   lua_pushnumber(L, isGLES);

   if (lua_pcall(L, 2, 0, 0) != 0)
   {
      fprintf(stderr, "[JOULEPLATING] ERROR in context_reset: %s\n", lua_tostring(L, -1));
   }

#ifdef CORE
   context_alive = true;
   init_multisample(multisample);
#endif
}

static void context_destroy(void)
{
   log_cb(RETRO_LOG_INFO, "[JOULEPLATING] Context destroy!\n");

#ifdef CORE
   init_multisample(0);
   context_alive = false;
#endif

   lua_State* L = m_Lua;
   lua_getglobal(L, "on_lua_exitgl");
   if (lua_pcall(L, 0, 0, 0) != 0)
   {
      log_cb(RETRO_LOG_ERROR, "[JOULEPLATING] ERROR in context_destroy: %s\n", lua_tostring(L, -1));
   }
}

#ifdef HAVE_OPENGLES
static bool retro_init_hw_context(void)
{
#if defined(HAVE_OPENGLES_3_1)
   hw_render.context_type = RETRO_HW_CONTEXT_OPENGLES_VERSION;
   hw_render.version_major = 3;
   hw_render.version_minor = 1;
#elif defined(HAVE_OPENGLES3)
   hw_render.context_type = RETRO_HW_CONTEXT_OPENGLES3;
#else
   hw_render.context_type = RETRO_HW_CONTEXT_OPENGLES2;
#endif
   hw_render.context_reset = context_reset;
   hw_render.context_destroy = context_destroy;
   hw_render.depth = true;
   hw_render.stencil = false;
   hw_render.bottom_left_origin = true;

   if (!environ_cb(RETRO_ENVIRONMENT_SET_HW_RENDER, &hw_render))
      return false;

   return true;
}
#else
static bool retro_init_hw_context(void)
{
#if defined(CORE)
   hw_render.context_type = RETRO_HW_CONTEXT_OPENGL_CORE;
   hw_render.version_major = 3;
   hw_render.version_minor = 1;
#else
   hw_render.context_type = RETRO_HW_CONTEXT_OPENGL;
#endif
   hw_render.context_reset = context_reset;
   hw_render.context_destroy = context_destroy;
   hw_render.depth = true;
   hw_render.stencil = true;
   hw_render.bottom_left_origin = true;

   if (!environ_cb(RETRO_ENVIRONMENT_SET_HW_RENDER, &hw_render))
      return false;

   return true;
}
#endif

bool retro_load_game(const struct retro_game_info *info)
{
   log_cb(RETRO_LOG_INFO, "[JOULEPLATING] retro_load_game[%s]\n", info->path);

   update_variables();

   enum retro_pixel_format fmt = RETRO_PIXEL_FORMAT_XRGB8888;
   if (!environ_cb(RETRO_ENVIRONMENT_SET_PIXEL_FORMAT, &fmt))
   {
      fprintf(stderr, "[JOULEPLATING] XRGB8888 is not supported.\n");
      return false;
   }

   struct retro_audio_callback audio_cb = { emit_audio, audio_set_state };
   use_audio_cb = environ_cb(RETRO_ENVIRONMENT_SET_AUDIO_CALLBACK, &audio_cb);

   if (!retro_init_hw_context())
   {
      fprintf(stderr, "[JOULEPLATING] HW Context could not be initialized, exiting...\n");
      return false;
   }
   
   if (info && info->path)
   {
      bool retval = lua_init(info->path);
      if (retval == false)
      {
         return false;
      }
   }

   log_cb(RETRO_LOG_INFO, "[JOULEPLATING] Loaded game!\n");
   (void)info;
   return true;
}

void retro_unload_game(void)
{}

unsigned retro_get_region(void)
{
   return RETRO_REGION_NTSC;
}

bool retro_load_game_special(unsigned type, const struct retro_game_info *info, size_t num)
{
   (void)type;
   (void)info;
   (void)num;
   return false;
}

size_t retro_serialize_size(void)
{
   return 0;
}

bool retro_serialize(void *data, size_t size)
{
   (void)data;
   (void)size;
   return false;
}

bool retro_unserialize(const void *data, size_t size)
{
   (void)data;
   (void)size;
   return false;
}

void *retro_get_memory_data(unsigned id)
{
   (void)id;
   return NULL;
}

size_t retro_get_memory_size(unsigned id)
{
   (void)id;
   return 0;
}

void retro_reset(void)
{}

void retro_cheat_reset(void)
{}

void retro_cheat_set(unsigned index, bool enabled, const char *code)
{
   (void)index;
   (void)enabled;
   (void)code;
}
