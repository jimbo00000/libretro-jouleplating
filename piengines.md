

# Performance Scores
| Engine | Bunnymark score |
| ------ | --------------- |
| Chailove | 7 |
| libretro-lutro | |
| Tilengine | 2048 |
| raylib |  x No bunnies displayed |
| pygame |   |



## Game Engines for Pi
- [Love2D](https://love2d.org/)
- [ChaiLove](https://github.com/libretro/libretro-chailove)
- [libretro-lutro](https://github.com/libretro/libretro-lutro)
- [Tilengine](https://github.com/megamarc/Tilengine)
- [raylib](https://github.com/raysan5/raylib)
- [Ouzel](https://github.com/elnormous/ouzel)
- [Kha](https://github.com/Kode/Kha)
- [Pyxel](https://github.com/kitao/pyxel)
- [Marshmallow](https://github.com/gamaral/marshmallow_h)
- [Starling](https://github.com/Gamua/Starling-Framework)
- [Castle Game Engine](https://castle-engine.io/index.php)
- [Amulet](https://github.com/ianmaclarty/amulet)
- [DIV Games Studio 2](https://github.com/DIVGAMES/DIV-Games-Studio)
- [OpenFL/Haxe](https://github.com/openfl/)
- [Urho3D](https://github.com/urho3d/Urho3D)
- [Godot](https://github.com/godotengine/godot)
- [AppGameKit](https://www.appgamekit.com/users#raspberry)
- [Sploder](http://www.sploder.com/)
- [PICO-8](https://www.lexaloffle.com/pico-8.php)
- [GB Studio](https://www.gbstudio.dev/)

### *Might* work
- [Lazarus](https://www.getlazarus.org/)
- [Defold](https://defold.com/)
- [xygine](https://github.com/fallahn/xygine)
- [luxe](https://luxeengine.com/)

### Raspbian
- [pygame](https://github.com/pygame/pygame)
- [pyglet](https://github.com/pyglet/pyglet)
- [pygame zero](https://pygame-zero.readthedocs.io/en/stable/)
- [Python Play](https://github.com/replit/play)
- [Processing](https://pi.processing.org/get-started/)
- [Scratch](https://www.raspberrypi.org/documentation/usage/scratch/)

## Physics
- [Chipmunk](https://github.com/slembcke/Chipmunk2D)
- [bump.lua](https://github.com/kikito/bump.lua)
- [box2d](https://github.com/erincatto/box2d)
   - [box2d-lite](https://github.com/erincatto/box2d-lite)
   - [PlayRho](https://github.com/louis-langholtz/PlayRho)
   - [liquidfun](http://google.github.io/liquidfun/)
- [ODE](https://www.ode.org/)
- [awesome-collision-detection](https://github.com/jslee02/awesome-collision-detection)

## Graphics
- [gles2framework](https://github.com/chriscamacho/gles2framework)
- [GeeXLab](https://www.geeks3d.com/geexlab/)
- [FBGraphics](https://github.com/grz0zrg/fbg)
- [tasty-pastry](https://github.com/going-digital/tasty-pastry)
- [glmark2](https://github.com/glmark2/glmark2)
- [Pi On Fire](http://www.deater.net/weave/vmwprod/pionfire/)

## RetroPie
- [RetroPie-Setup](https://github.com/RetroPie/RetroPie-Setup)
- [RetroPie-Extra](https://github.com/zerojay/RetroPie-Extra)
- [Lakka](https://www.lakka.tv/)

## OS
- [VMWos](https://github.com/deater/vmwos)
- [Ultibo](https://ultibo.org/)
- [PiBakery](https://github.com/davidferguson/pibakery/)

## Pi-specific libraries
- [pi3d](https://github.com/tipam/pi3d)
- [rpi](https://github.com/mmozeiko/rpi)
- [QPULib](https://github.com/mn416/QPULib)
- [videocoreiv-qpu](https://github.com/hermanhermitage/videocoreiv-qpu)
- [rpi-open-firmware](https://github.com/christinaa/rpi-open-firmware)
- [Pi Bare Metal ASM](https://github.com/PeterLemon/RaspberryPi)
- [libcec](https://github.com/Pulse-Eight/libcec)
- [PiTubeDirect](https://github.com/hoglet67/PiTubeDirect)
- [Bare Metal Programming on Pi](https://github.com/bztsrc/raspi3-tutorial)
- [pi userland](https://github.com/raspberrypi/userland)

## Cross Compilers
- [rpi-buildroot](https://github.com/gamaral/rpi-buildroot)
- [Bsquask SDK](https://github.com/nezticle/RaspberryPi-BuildRoot)
- [vc4-toolchain](https://github.com/itszor/vc4-toolchain)


https://en.wikipedia.org/wiki/VideoCore

https://github.com/hermanhermitage/videocoreiv/wiki/VideoCore-IV---BCM2835-Overview

https://dri.freedesktop.org/docs/drm/gpu/vc4.html

https://www.bigmessowires.com/2014/10/06/raspberry-pi-3d-performance-demo/

https://upcommons.upc.edu/bitstream/handle/2117/106767/Optimisation%20Opportunities%20and%20Evaluation%20for.pdf

https://github.com/phire/hackdriver

https://github.com/anholt/vc4-gpu-tools/blob/master/tools/vc4_qpu_disasm.c


----------------------------------------------
# Notes
https://castle-engine.io/wp/2019/08/25/castle-game-engine-on-raspberry-pi/

http://www.gepatto.nl/openfl47-raspberrypi/

https://www.drdobbs.com/embedded-systems/writing-a-particle-system-on-the-raspber/240163294

https://www.raspberrypi.org/blog/a-birthday-present-from-broadcom/

https://www.raspberrypi.org/blog/accelerating-fourier-transforms-using-the-gpu/

https://docs.broadcom.com/docs/12358545

https://youtu.be/hNeq-iG9pfc?t=2421 - 16 bit floats

https://pet.timetocode.org/raspberrypi.html#physics

