#!/bin/bash

CORE_HOME='/opt/retropie/libretrocores/lr-jouleplating'
ROMS_HOME='/home/pi/RetroPie/roms/jouleplating'
CFGS_HOME='/opt/retropie/configs/jouleplating'

if [ ! -d "$ROMS_HOME" ];then
    echo "directory $ROMS_HOME doesn't exist."
    exit
fi

echo "Placing data and code in $ROMS_HOME"
cp -Rv data $ROMS_HOME/
cp -Rv util $ROMS_HOME/
cp -Rv scene_es2/* $ROMS_HOME/

# Startup files copied in by lr- install scriptmodule
#echo "Copying startup files..."
#cp jouleplating.lua $ROMS_HOME
#cp opengles2.lua $ROMS_HOME

echo "Copying es_systems.cfg..."
diff es_systems.cfg /etc/emulationstation/es_systems.cfg
cp es_systems.cfg /opt/retropie/configs/all/emulationstation/

echo "Copying themes..."
cp -Rv /etc/emulationstation/themes /opt/retropie/configs/all/emulationstation/
cp -Rv themes /opt/retropie/configs/all/emulationstation/
