// glfw3_boilerplate.cpp

#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include <GLFW/glfw3.h>
extern "C" {
  #include <lua.h>
  #include "lualib.h"
  #include "lauxlib.h"
}

GLFWwindow* g_pWindow = NULL;
lua_State* m_Lua = NULL;

unsigned frame_count = 0;
unsigned width  = 800;
unsigned height = 600;

// http://stackoverflow.com/questions/4508119/redirecting-redefining-print-for-embedded-lua
static int l_my_print(lua_State* L) {
   int nargs = lua_gettop(L);

   fprintf(stderr, "[LUAJIT]: ");
   for (int i=1; i <= nargs; i++) {
      if (lua_isstring(L, i)) {
         const char* pStr = lua_tostring(L, i);
         fprintf(stderr, "%s\t", pStr);
      }
      else {
         /* Do something with non-strings if you like */
      }
   }
   fprintf(stderr, "\n");

   return 0;
}

static const struct luaL_Reg printlib [] = {
    {"print", l_my_print},
    {NULL, NULL} /* end of array */
};

extern void luaopen_luamylib(lua_State *L)
{
    lua_getglobal(L, "_G");
    luaL_register(L, NULL, printlib);
    lua_pop(L, 1);
}

void jouleplating_init(
    const char* gamePath,
    const char* startupPath,
    const char* codePath,
    const char* dataPath)
{
   fprintf(stderr, "[JOULEPLATING] lua_init(%s)\n", gamePath);

   m_Lua = luaL_newstate();

   luaL_openlibs(m_Lua);
   if (m_Lua == NULL)
   {
      fprintf(stderr, "[JOULEPLATING] ERROR: lua open libs\n");
   }

   lua_State *L = m_Lua;
   luaopen_luamylib(L);
   
   fprintf(stderr, "[JOULEPLATING] setting startup path=%s\n", startupPath);
   
   char strbuf[512];
   snprintf(strbuf, 512, "package.path = package.path .. ';%s/?.lua'", startupPath);
   luaL_dostring(L, strbuf);
   luaL_dostring(L, "print(package.path)");

   snprintf(strbuf, 512, "datadir = '%s'", dataPath);
   luaL_dostring(L, strbuf);

   snprintf(strbuf, 512, "%s/jouleplating.lua", startupPath);
   fprintf(stderr, "[JOULEPLATING] luaL_loadfile(%s)\n", strbuf);
   
   int errCode = luaL_loadfile(L, strbuf);
   switch (errCode)
   {
      case 0:
      break;
      
      case LUA_ERRSYNTAX:
      fprintf(stderr, "[JOULEPLATING] LUA_ERRSYNTAX\n");
      break;

      case LUA_ERRMEM:
      fprintf(stderr, "[JOULEPLATING] LUA_ERRMEM\n");
      break;

      case LUA_ERRFILE:
      fprintf(stderr, "[JOULEPLATING] LUA_ERRFILE\n");
      break;

      default:
      fprintf(stderr, "[JOULEPLATING] Unknown error\n");
      break;
   }

   fprintf(stderr, "[JOULEPLATING] lua_pcall\n");
   errCode = lua_pcall(L, 0, LUA_MULTRET, 0);
   switch (errCode)
   {
      case 0:
      break;
      
      case LUA_ERRRUN:
      fprintf(stderr, "[JOULEPLATING] LUA_ERRRUN\n");
      break;

      case LUA_ERRMEM:
      fprintf(stderr, "[JOULEPLATING] LUA_ERRMEM\n");
      break;

      case LUA_ERRERR:
      fprintf(stderr, "[JOULEPLATING] LUA_ERRERR\n");
      break;

      default:
      fprintf(stderr, "[JOULEPLATING] pcall Unknown error\n");
      break;
   }


   // Inform Lua of paths to code and data
   lua_getglobal(L, "lua_set_directories");
   lua_pushstring(L, startupPath);
   lua_pushstring(L, dataPath);
   lua_pushstring(L, codePath);
   if (lua_pcall(L, 3, 0, 0) != 0)
   {
      fprintf(stderr, "[JOULEPLATING] ERROR in lua_set_directories: %s\n", lua_tostring(L, -1));
   }

   
   // Inform Lua of game name
   lua_getglobal(L, "on_lua_change_game");
   lua_pushstring(L, gamePath);
   if (lua_pcall(L, 1, 0, 0) != 0)
   {
      fprintf(stderr, "[JOULEPLATING] ERROR in on_lua_change_game: %s\n", lua_tostring(L, -1));
   }

   //lutro_audio_preload(L);
}

void jouleplating_deinit(lua_State* L)
{
   if (L != NULL)
   {
      lua_close(L);
   }
}

void jouleplating_context_reset(lua_State* L)
{
   // Inform Lua of viewport size
   lua_getglobal(L, "on_lua_resize_viewport");
   lua_pushnumber(L, width);
   lua_pushnumber(L, height);
   if (lua_pcall(L, 2, 0, 0) != 0)
   {
      fprintf(stderr, "[JOULEPLATING] ERROR in on_lua_resize_viewport: %s\n", lua_tostring(L, -1));
   }

   lua_getglobal(L, "on_lua_initgl");
   // Pass in a (GL function loader) function pointer.
   intptr_t loader = (intptr_t)glfwGetProcAddress;
   lua_Number LpLoaderFunc = (double)loader;
   lua_pushnumber(L, LpLoaderFunc);

   // Choose desktop or ES OpenGL
   int isGLES = 0;
#ifdef HAVE_OPENGLES
   isGLES = 1;
#endif
   lua_pushnumber(L, isGLES);

   if (lua_pcall(L, 2, 0, 0) != 0)
   {
      fprintf(stderr, "[JOULEPLATING] ERROR in context_reset: %s\n", lua_tostring(L, -1));
   }
}

void jouleplating_context_destroy(lua_State* L)
{
   fprintf(stderr, "[JOULEPLATING] Context destroy!\n");

   lua_getglobal(L, "on_lua_exitgl");
   if (lua_pcall(L, 0, 0, 0) != 0)
   {
      fprintf(stderr, "[JOULEPLATING] ERROR in context_destroy: %s\n", lua_tostring(L, -1));
   }
}

void jouleplating_display(lua_State* L)
{
    lua_getglobal(L, "on_lua_drawframe");
    lua_pushnumber(L, frame_count);
    if (lua_pcall(L, 1, 0, 0) != 0)
    {
        fprintf(stderr, "[JOULEPLATING] ERROR in on_lua_drawframe: %s\n", lua_tostring(L, -1));
        //assert(false);
        exit(0);
    }
}

///@return 0 for success, non-zero otherwise
int initGL(int argc, char** argv)
{
    // set path for jouleplating.lua and opengles2.lua
    const char* startupPath = "../..";
    const char* codePath = "../..";
    const char* dataPath = "../../data";

    const char* sceneDir = "scene_es2";
    const char* sceneName = "simple_clockface";
    
    char pathBuf[512];
    if (argc > 1)
    {
        sprintf(pathBuf, "%s", argv[1]);
    }
    else
    {
        sprintf(pathBuf, "%s/%s/%s.lua", startupPath, sceneDir, sceneName);
    }
    jouleplating_init(pathBuf, startupPath, codePath, dataPath);
    jouleplating_context_reset(m_Lua);

    return 0;
}

int main(int argc, char *argv[])
{
    if (!glfwInit())
    {
        return -1;
    }

    g_pWindow = glfwCreateWindow(width, height, "GL Boilerplate", 0, 0);
    glfwMakeContextCurrent(g_pWindow);
    glfwSwapInterval(1);

    if (initGL(argc, argv) != 0)
    {
        return 0;
    }

    while (glfwWindowShouldClose(g_pWindow) == false)
    {
        jouleplating_display(m_Lua);
        frame_count++;
        glfwPollEvents();
        ///@todo Inform lua of input state
        glfwSwapBuffers(g_pWindow);
    }

    jouleplating_context_destroy(m_Lua);
    glfwTerminate();
    return 0;
}
