-- glfont.lua

require("opengl")
local bmf = require("util.bmfont")
local ffi = require("ffi")
local sf = require("util.shaderfunctions2")
local filesystem = require 'util.filesystem'

-- this is our GLFont class
local GLFont = {}
GLFont.__index = GLFont

-- and its new function
function GLFont.new(...)
    local self = setmetatable({}, GLFont)
    if self.init ~= nil and type(self.init) == "function" then
        self:init(...)
    end 
    return self
end

function GLFont:init(fontfile, imagefile, texw, texh, texd, texformat)
    self.fontfile = fontfile
    self.imagefile = imagefile
    self.tex_w = texw or 512
    self.tex_h = texh or 512
    self.tex_d = texd or 4
    self.tex_format = texformat or GL.GL_RGBA
    -- read glyphs from font.txt and store them in chars table
    self.chars = {}
    self.vao = 0
    self.prog = 0
    self.tex = 0
    self.vbos = {}
    self.string_vbo_table = {}
    self.dataDir = nil
end

function GLFont:setDataDirectory(dir)
    --self.dataDir = dir
end

local font_gles20_vert = [[
#version 100

attribute vec4 vPosition;
attribute vec4 vColor;
varying vec3 vfColor;

uniform mat4 mmtx;
uniform mat4 vmtx;
uniform mat4 prmtx;

void main()
{
    vfColor = vColor.xyz;
    gl_Position = prmtx * vmtx * mmtx * vPosition;
}
]]

local font_gles20_frag = [[
#version 100

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec3 vfColor;

uniform sampler2D tex;
uniform vec3 uColor;

float smoothing = 1.0/32.0;

void main()
{
#if 0
    vec3 texCol = texture2D(tex, vfColor.xy).xyz;
    float lum = length(texCol);
    gl_FragColor = vec4(uColor, lum);
    
#else
    vec4 col = vec4(uColor, 1.);
    float mask = texture2D(tex, vfColor.xy).g;
    float alpha = smoothstep(0.5 - smoothing, 0.5 + smoothing, mask);

    gl_FragColor = vec4(col.rgb, col.a * alpha);
#endif
}
]]

-- http://wiki.interfaceware.com/534.html
local function string_split(s, d)
    local t = {}
    local i = 0
    local f
    local match = '(.-)' .. d .. '()'
    
    if string.find(s, d) == nil then
        return {s}
    end
    
    for sub, j in string.gmatch(s, match) do
        i = i + 1
        t[i] = sub
        f = j
    end
    
    if i ~= 0 then
        t[i+1] = string.sub(s, f)
    end
    
    return t
end

function GLFont:initGL()
    -- Use VAOs if OpenGL version supports them
    local GLversionString = ffi.string(gl.glGetString(GL.GL_VERSION))
    local tokens = string_split(GLversionString, '%.') -- escape .
    local major,minor = tokens[1], tokens[2]
    if major == '4' then
        self.DO_VAOS = true
    end

    if self.DO_VAOS then
        local vaoId = ffi.new("int[1]")
        gl.glGenVertexArrays(1, vaoId)
        self.vao = vaoId[0]
        gl.glBindVertexArray(self.vao)
    end

    self.prog = sf.make_shader_from_source({
        vsrc = font_gles20_vert,
        fsrc = font_gles20_frag,
        })

    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog, "vColor")

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)
    if self.DO_VAOS then
        gl.glBindVertexArray(0)
    end

    local texId = ffi.new("GLuint[1]")
    gl.glGenTextures(1, texId);
    self.tex = texId[0]

    -- $ convert papyrus_512_0.png  -size 512x512 -depth 32 -channel RGBA gray:papyrus_512_0.raw
    local fontname, texname, tw, th, td, format =
        self.fontfile, self.imagefile, self.tex_w, self.tex_h, self.tex_d, self.tex_format
    --if self.dataDir then fontname = self.dataDir .. "/" .. fontname end
    --if self.dataDir then texname = self.dataDir .. "/" .. texname end
    fontname = 'fonts/'..fontname
    texname = 'fonts/'..texname

    self.font = bmf.new(fontname, nil)

    local data, size = filesystem.read(texname)
    print(size, 'bytes read from', texname)
    if data then
        local glCharv = ffi.typeof('GLchar[?]')
        local pixels = glCharv(self.tex_w*self.tex_h*td, data)

        gl.glBindTexture(GL.GL_TEXTURE_2D, self.tex)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR)
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR)
        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, format, self.tex_w, self.tex_h, 0, format, GL.GL_UNSIGNED_BYTE, pixels)
        gl.glBindTexture(GL.GL_TEXTURE_2D, 0)
    end

    gl.glDisableVertexAttribArray(vpos_loc)
    gl.glDisableVertexAttribArray(vcol_loc)
end

function GLFont:exitGL()
    for k,v in pairs(self.vbos) do
        gl.glDeleteBuffers(1,v)
    end

    local texdel = ffi.new("GLuint[1]", self.tex)
    gl.glDeleteTextures(1,texdel)

    self.vbos = {}
    gl.glDeleteProgram(self.prog)

    for k,v in pairs(self.string_vbo_table) do
        local vv,vt = v[1], v[2]
        gl.glDeleteBuffers(1,vv)
        gl.glDeleteBuffers(1,vt)
    end

    if self.DO_VAOS then
        local vaoId = ffi.new("GLuint[1]", self.vao)
        gl.glDeleteVertexArrays(1, vaoId)
        gl.glBindVertexArray(0)
    end

    --self.font.chars = {}
end

function GLFont:bindArrays()
    if self.DO_VAOS then
        gl.glBindVertexArray(self.vao)
    end
end

function GLFont:unbindArrays()
    if self.DO_VAOS then
        gl.glBindVertexArray(0)
    end
end

function GLFont:render_string(mview, proj, color, str)
    local m = {
        1,0,0,0,
        0,1,0,0,
        0,0,1,0,
        0,0,0,1,
    }
    self:render_string2(m, mview, proj, color, str)
end

function GLFont:render_string2(model, view, proj, color, str)
    if not str then return end
    if #str == 0 then return end
    if self.prog == 0 then return end

    local um_loc = gl.glGetUniformLocation(self.prog, "mmtx")
    local uv_loc = gl.glGetUniformLocation(self.prog, "vmtx")
    local upr_loc = gl.glGetUniformLocation(self.prog, "prmtx")
    local ut_loc = gl.glGetUniformLocation(self.prog, "tex")
    local uc_loc = gl.glGetUniformLocation(self.prog, "uColor")

    local glIntv   = ffi.typeof('GLint[?]')
    local glFloatv = ffi.typeof('GLfloat[?]')

    gl.glUseProgram(self.prog)
    gl.glUniformMatrix4fv(um_loc, 1, GL.GL_FALSE, glFloatv(16, model))
    gl.glUniformMatrix4fv(uv_loc, 1, GL.GL_FALSE, glFloatv(16, view))
    gl.glUniformMatrix4fv(upr_loc, 1, GL.GL_FALSE, glFloatv(16, proj))

    gl.glActiveTexture(GL.GL_TEXTURE0)
    gl.glBindTexture(GL.GL_TEXTURE_2D, self.tex)
    gl.glUniform1i(ut_loc, 0)

    gl.glUniform3f(uc_loc, color[1], color[2], color[3])

    gl.glEnable(GL.GL_BLEND)
    gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)

    self:bindArrays()

    local vpos_loc = gl.glGetAttribLocation(self.prog, "vPosition")
    local vcol_loc = gl.glGetAttribLocation(self.prog, "vColor")

    if self.string_vbo_table[str] == nil then
        --print("First time seeing "..str)
        -- Accumulate a whole string's worth of vertex array data.
        local stringv = {}
        local stringt = {}

        x,y = 0,0
        for i=1,#str do
            local ch = str:byte(i)
            if ch ~= nil then
                local v, t, xa = self.font:getcharquad(ch, x, y, self.tex_w, self.tex_h)
                if v and t then
                    local quadv = {
                        v[1], v[2], v[3], v[4], v[5], v[6],
                        v[5], v[6], v[7], v[8], v[1], v[2],
                    }
                    local quadt = {
                        t[1], t[2], t[3], t[4], t[5], t[6],
                        t[5], t[6], t[7], t[8], t[1], t[2],
                    }
                    for i=1,#quadv do table.insert(stringv, quadv[i]) end
                    for i=1,#quadt do table.insert(stringt, quadt[i]) end
                    x = x + xa
                end
            end
        end

        if #stringv > 0 then
            local verts = glFloatv(#stringv, stringv)
            local texs  = glFloatv(#stringt, stringt)

            local newvbov = glIntv(1)
            gl.glGenBuffers(1, newvbov)
            gl.glBindBuffer(GL.GL_ARRAY_BUFFER, newvbov[0])
            gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(verts), verts, GL.GL_STATIC_DRAW)
            gl.glVertexAttribPointer(vpos_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
            table.insert(self.vbos, newvbov)

            local newvbot = glIntv(1)
            gl.glGenBuffers(1, newvbot)
            gl.glBindBuffer(GL.GL_ARRAY_BUFFER, newvbot[0])
            gl.glBufferData(GL.GL_ARRAY_BUFFER, ffi.sizeof(texs), texs, GL.GL_STATIC_DRAW)
            gl.glVertexAttribPointer(vcol_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
            table.insert(self.vbos, newvbot)

            self.string_vbo_table[str] = {newvbov, newvbot}
        else
            self:unbindArrays()
            gl.glDisable(GL.GL_BLEND)
            gl.glUseProgram(0)
            return
        end
    else
        local strVBO = self.string_vbo_table[str]
        strVBO.age = 0
        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, strVBO[1][0])
        gl.glVertexAttribPointer(vpos_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
        gl.glBindBuffer(GL.GL_ARRAY_BUFFER, strVBO[2][0])
        gl.glVertexAttribPointer(vcol_loc, 2, GL.GL_FLOAT, GL.GL_FALSE, 0, nil)
    end

    gl.glEnableVertexAttribArray(vpos_loc)
    gl.glEnableVertexAttribArray(vcol_loc)

    gl.glDrawArrays(GL.GL_TRIANGLES, 0, 3*2*#str)

    self:unbindArrays()
    gl.glDisable(GL.GL_BLEND)

    gl.glUseProgram(0)
end

function GLFont:get_string_width(str)
    if #str == 0 then return 0 end

    local w = 0
    for i=1,#str do
        local ch = str:byte(i)
        if ch ~= nil then
            local v, t, xa = self.font:getcharquad(ch, 0,0, self.tex_w, self.tex_h)
            if v and t then
                w = w + xa
            end
        end
    end
    return w
end

function GLFont:get_max_char_width()
    local mx = 0
    for k,v in pairs(self.font.chars) do
        mx = math.max(mx, v.xadvance)
    end
    return mx
end

function GLFont:stringcount()
    local count = 0
    for _ in pairs(self.string_vbo_table) do count = count + 1 end
    return count
end

function GLFont:deleteoldstrings()
    for k,v in pairs(self.string_vbo_table) do
        if v.age and v.age > 100 then
            local vv,vt = v[1], v[2]
            gl.glDeleteBuffers(1,vv)
            gl.glDeleteBuffers(1,vt)
            self.string_vbo_table[k] = nil
        end
        if v.age then
            v.age = v.age + 1 or 1
        else
            v.age = 1
        end
    end
end

return GLFont
