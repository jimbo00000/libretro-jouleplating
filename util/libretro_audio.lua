--[[ libretro_audio.lua

    Handles sound using native audio code from libretro-lutro.
    Designed to be API compatible with soundfx.lua which calls into Bass.
]]
libretro_audio = {}

-- Depends on global table 'audio'

local samples = {}

function libretro_audio.setDataDirectory(dir)
    libretro_audio.data_dir = dir
end

function libretro_audio.playSound(filename)
    if not audio then return end

    if not samples[filename] then
        local audio_dir = libretro_audio.data_dir..'/'..'sounds'
        local fullpath = audio_dir..'/'..filename
        samples[filename] = audio.newSource(fullpath)
    end
    -- Registered in function lutro_audio_preload in audio.c
    audio.play(samples[filename])
end

return libretro_audio
