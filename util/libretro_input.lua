-- libretro_input.lua
-- Maps SDL button indices to Libretro's Retropad.

libretro_input = {}

libretro_input.JOYPAD_B      =  0+1
libretro_input.JOYPAD_Y      =  1+1
libretro_input.JOYPAD_SELECT =  2+1
libretro_input.JOYPAD_START  =  3+1
libretro_input.JOYPAD_UP     =  4+1
libretro_input.JOYPAD_DOWN   =  5+1
libretro_input.JOYPAD_LEFT   =  6+1
libretro_input.JOYPAD_RIGHT  =  7+1
libretro_input.JOYPAD_A      =  8+1
libretro_input.JOYPAD_X      =  9+1
libretro_input.JOYPAD_L      = 10+1
libretro_input.JOYPAD_R      = 11+1
libretro_input.JOYPAD_L2     = 12+1
libretro_input.JOYPAD_R2     = 13+1
libretro_input.JOYPAD_L3     = 14+1
libretro_input.JOYPAD_R3     = 15+1


libretro_input.mappings = {
    ['USB Gamepad '] = {
        [1] = libretro_input.JOYPAD_X,
        [2] = libretro_input.JOYPAD_A,
        [3] = libretro_input.JOYPAD_B,
        [4] = libretro_input.JOYPAD_Y,
        [5] = libretro_input.JOYPAD_L,
        [6] = libretro_input.JOYPAD_R,
        [9] = libretro_input.JOYPAD_SELECT,
        [10] = libretro_input.JOYPAD_START,
    },
    ['USB,2-axis 8-button gamepad  '] = {
        [1] = libretro_input.JOYPAD_A,
        [2] = libretro_input.JOYPAD_B,
        [3] = libretro_input.JOYPAD_X,
        [4] = libretro_input.JOYPAD_Y,
        [5] = libretro_input.JOYPAD_L,
        [6] = libretro_input.JOYPAD_R,
        [7] = libretro_input.JOYPAD_SELECT,
        [8] = libretro_input.JOYPAD_START,
    },
    ['SNES30             SNES30 Joy    '] = {
        [1] = libretro_input.JOYPAD_A,
        [2] = libretro_input.JOYPAD_B,
        [4] = libretro_input.JOYPAD_X,
        [5] = libretro_input.JOYPAD_Y,
        [7] = libretro_input.JOYPAD_L,
        [8] = libretro_input.JOYPAD_R,
        [11] = libretro_input.JOYPAD_SELECT,
        [12] = libretro_input.JOYPAD_START,
    },
    ['Microsoft X-Box 360 pad'] = {
        [1] = libretro_input.JOYPAD_B,
        [2] = libretro_input.JOYPAD_A,
        [3] = libretro_input.JOYPAD_Y,
        [4] = libretro_input.JOYPAD_X,
        [5] = libretro_input.JOYPAD_L,
        [6] = libretro_input.JOYPAD_R,
        [7] = libretro_input.JOYPAD_SELECT,
        [8] = libretro_input.JOYPAD_START,
    },
    ['Retr'] = {
        [1] = libretro_input.JOYPAD_B,
        [2] = libretro_input.JOYPAD_A,
        [3] = libretro_input.JOYPAD_SELECT,
        [4] = libretro_input.JOYPAD_START,
    },
    ['GamePad Pro USB '] = {
        [1] = libretro_input.JOYPAD_Y,
        [2] = libretro_input.JOYPAD_B,
        [3] = libretro_input.JOYPAD_A,
        [4] = libretro_input.JOYPAD_X,
        [5] = libretro_input.JOYPAD_L,
        [6] = libretro_input.JOYPAD_R,
        [9] = libretro_input.JOYPAD_SELECT,
        [10] = libretro_input.JOYPAD_START,
    },
    ['Gravis GamePad Pro USB '] = {
        [1] = libretro_input.JOYPAD_X,
        [2] = libretro_input.JOYPAD_B,
        [3] = libretro_input.JOYPAD_A,
        [4] = libretro_input.JOYPAD_Y,
        [5] = libretro_input.JOYPAD_L,
        [6] = libretro_input.JOYPAD_R,
        [9] = libretro_input.JOYPAD_SELECT,
        [10] = libretro_input.JOYPAD_START,
    },

    -- Keyboard layouts
    -- One nicely spaced out version on WASD
    ['keyboard'] = {
        ['i'] = libretro_input.JOYPAD_X,
        ['j'] = libretro_input.JOYPAD_Y,
        ['k'] = libretro_input.JOYPAD_B,
        ['l'] = libretro_input.JOYPAD_A,
        ['u'] = libretro_input.JOYPAD_L,
        ['o'] = libretro_input.JOYPAD_R,
        ['w'] = libretro_input.JOYPAD_UP,
        ['s'] = libretro_input.JOYPAD_DOWN,
        ['a'] = libretro_input.JOYPAD_LEFT,
        ['d'] = libretro_input.JOYPAD_RIGHT,
        ['v'] = libretro_input.JOYPAD_SELECT,
        ['b'] = libretro_input.JOYPAD_START,
    },

    -- Two horizontally spaced out layouts one over the other
    ['keyboard1'] = {
        ['8'] = libretro_input.JOYPAD_X,
        ['u'] = libretro_input.JOYPAD_Y,
        ['i'] = libretro_input.JOYPAD_B,
        ['o'] = libretro_input.JOYPAD_A,
        ['7'] = libretro_input.JOYPAD_L,
        ['9'] = libretro_input.JOYPAD_R,
        ['2'] = libretro_input.JOYPAD_UP,
        ['w'] = libretro_input.JOYPAD_DOWN,
        ['q'] = libretro_input.JOYPAD_LEFT,
        ['e'] = libretro_input.JOYPAD_RIGHT,
        ['t'] = libretro_input.JOYPAD_SELECT,
        ['y'] = libretro_input.JOYPAD_START,
    },
    ['keyboard2'] = {
        ['k'] = libretro_input.JOYPAD_X,
        ['m'] = libretro_input.JOYPAD_Y,
        [','] = libretro_input.JOYPAD_B,
        ['.'] = libretro_input.JOYPAD_A,
        ['j'] = libretro_input.JOYPAD_L,
        ['l'] = libretro_input.JOYPAD_R,
        ['s'] = libretro_input.JOYPAD_UP,
        ['x'] = libretro_input.JOYPAD_DOWN,
        ['z'] = libretro_input.JOYPAD_LEFT,
        ['c'] = libretro_input.JOYPAD_RIGHT,
        ['v'] = libretro_input.JOYPAD_SELECT,
        ['b'] = libretro_input.JOYPAD_START,
    },

    -- Compact layout to fir as many controllers as possible
    -- on a single laptop keyboard for testing
    ['compact_keyboard1'] = {
        ['a'] = libretro_input.JOYPAD_UP,
        ['z'] = libretro_input.JOYPAD_DOWN,
        ['s'] = libretro_input.JOYPAD_LEFT,
        ['x'] = libretro_input.JOYPAD_RIGHT,
        ['d'] = libretro_input.JOYPAD_B,
        ['f'] = libretro_input.JOYPAD_A,
        ['c'] = libretro_input.JOYPAD_SELECT,
        ['v'] = libretro_input.JOYPAD_START,
    },
    ['compact_keyboard2'] = {
        ['1'] = libretro_input.JOYPAD_UP,
        ['q'] = libretro_input.JOYPAD_DOWN,
        ['2'] = libretro_input.JOYPAD_LEFT,
        ['w'] = libretro_input.JOYPAD_RIGHT,
        ['3'] = libretro_input.JOYPAD_B,
        ['4'] = libretro_input.JOYPAD_A,
        ['e'] = libretro_input.JOYPAD_SELECT,
        ['r'] = libretro_input.JOYPAD_START,
    },
    ['compact_keyboard3'] = {
        ['g'] = libretro_input.JOYPAD_UP,
        ['b'] = libretro_input.JOYPAD_DOWN,
        ['h'] = libretro_input.JOYPAD_LEFT,
        ['n'] = libretro_input.JOYPAD_RIGHT,
        ['j'] = libretro_input.JOYPAD_B,
        ['k'] = libretro_input.JOYPAD_A,
        ['m'] = libretro_input.JOYPAD_SELECT,
        [','] = libretro_input.JOYPAD_START,
    },
    ['compact_keyboard4'] = {
        ['5'] = libretro_input.JOYPAD_UP,
        ['t'] = libretro_input.JOYPAD_DOWN,
        ['6'] = libretro_input.JOYPAD_LEFT,
        ['y'] = libretro_input.JOYPAD_RIGHT,
        ['7'] = libretro_input.JOYPAD_B,
        ['8'] = libretro_input.JOYPAD_A,
        ['u'] = libretro_input.JOYPAD_SELECT,
        ['i'] = libretro_input.JOYPAD_START,
    },
    ['compact_keyboard5'] = {
        ['9'] = libretro_input.JOYPAD_UP,
        ['o'] = libretro_input.JOYPAD_DOWN,
        ['0'] = libretro_input.JOYPAD_LEFT,
        ['p'] = libretro_input.JOYPAD_RIGHT,
        ['-'] = libretro_input.JOYPAD_B,
        ['='] = libretro_input.JOYPAD_A,
        ['['] = libretro_input.JOYPAD_SELECT,
        [']'] = libretro_input.JOYPAD_START,
    },
}

-- Jam SDL input state into Retropad state
function libretro_input.getRetropadStateFromSDL(name, axes, buttons)
    local rp = {}
    for i=1,14 do
        rp[i] = 0
    end
    local map = libretro_input.mappings['USB Gamepad ']
    if libretro_input.mappings[name] then map = libretro_input.mappings[name] end
    for i=1,math.min(14,#buttons) do
        --print(name, i,buttons[i])
        local RPi = map[i]
        if RPi then rp[RPi] = buttons[i] end
    end

    -- Axes
    if #axes == 5 then
        -- TODO: the first 4 axes are x, the 5th y? Seems wrong...
        local xshort = axes[1]
        local yshort = axes[5]
        local thresh = 1000
        if     xshort < -thresh then rp[libretro_input.JOYPAD_LEFT] = 1
        elseif xshort > thresh then rp[libretro_input.JOYPAD_RIGHT] = 1 end
        if     yshort < -thresh then rp[libretro_input.JOYPAD_UP] = 1
        elseif yshort > thresh then rp[libretro_input.JOYPAD_DOWN] = 1 end
    elseif #axes == 4 then
        local xshort = axes[1]
        local yshort = axes[2]
        local thresh = 1000
        if     xshort < -thresh then rp[libretro_input.JOYPAD_LEFT] = 1
        elseif xshort > thresh then rp[libretro_input.JOYPAD_RIGHT] = 1 end
        if     yshort < -thresh then rp[libretro_input.JOYPAD_UP] = 1
        elseif yshort > thresh then rp[libretro_input.JOYPAD_DOWN] = 1 end
    elseif #axes == 2 then -- 'USB,2-axis 8-button gamepad'
        local xshort = axes[1]
        local yshort = axes[2]
        local thresh = 1000
        if     xshort < -thresh then rp[libretro_input.JOYPAD_LEFT] = 1
        elseif xshort > thresh then rp[libretro_input.JOYPAD_RIGHT] = 1 end
        if     yshort < -thresh then rp[libretro_input.JOYPAD_UP] = 1
        elseif yshort > thresh then rp[libretro_input.JOYPAD_DOWN] = 1 end
    end

    return rp
end

return libretro_input
